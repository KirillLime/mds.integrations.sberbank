﻿using System.Collections.Generic;

namespace Mds.Integrations.Sberbank.Settings.Interfaces
{
    public interface IParameters
    {
        Dictionary<string, object> CollectParameters();
    }
}
