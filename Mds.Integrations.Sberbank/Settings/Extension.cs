﻿using Mds.Integrations.Sberbank.Settings.Interfaces;
using System;
using System.Collections.Generic;

namespace Mds.Integrations.Sberbank.Settings
{
    internal static class Extension
    {
        public static void AddNotNull<TValue>(this Dictionary<string, object> source, string key, TValue? value) where TValue : struct
        {
            if (value.HasValue)
            {
                var type = typeof(TValue);
                if (type.IsEnum)
                {
                    var intValue = Convert.ToInt32(value.Value);
                    source.Add(key, $"{intValue}");
                }
                else if (type.IsPrimitive)
                {
                    source.Add(key, $"{value}");
                }
            }
        }
        public static void AddNotNull(this Dictionary<string, object> source, string key, IValueParameter parameter)
        {
            source.AddNotNull(key, parameter?.Value);
        }
        public static void AddNotNull(this Dictionary<string, object> source, string key, IParameters value)
        {
            var values = value?.CollectParameters();
            if (null != values)
            {
                source.Add(key, values);
            }
        }
        public static void AddNotNull(this Dictionary<string, object> source, string key, string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                source.Add(key, value);
            }
        }
    }
}
