﻿using Mds.Integrations.Sberbank.Settings.Interfaces;
using System.Collections.Generic;

namespace Mds.Integrations.Sberbank.Settings
{
    public class AuthSettings : IParameters
    {
        public class Keys
        {
            public const string UserName = "userName";
            public const string Password = "password";
        }

        public string UserName { get; set; }
        public string Password { get; set; }



        public AuthSettings(string username, string password)
        {
            UserName = username;
            Password = password;
        }



        Dictionary<string, object> IParameters.CollectParameters()
        {
            var result = new Dictionary<string, object>();

            result.Add(Keys.UserName, UserName);
            result.Add(Keys.Password, Password);

            return result;
        }
    }
}
