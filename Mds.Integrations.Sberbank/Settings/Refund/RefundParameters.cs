﻿using Mds.Integrations.Sberbank.Settings.Interfaces;
using Mds.Integrations.Sberbank.Tools.Amount;
using System.Collections.Generic;

namespace Mds.Integrations.Sberbank.Settings.Refund
{
    public class RefundParameters : IParameters
    {
        public static class Keys
        {
            public static readonly string OrderId = "orderId";
            public static readonly string Amount = "amount";
        }
        public string OrderId { get; set; }
        public Price Amount { get; set; }


        public RefundParameters() : this(string.Empty, Price.Zero) { }
        public RefundParameters(string orderId, Price amount)
        {
            OrderId = orderId;
            Amount = amount;
        }



        Dictionary<string, object> IParameters.CollectParameters()
        {
            var result = new Dictionary<string, object>();

            result.Add(Keys.OrderId, OrderId);
            result.Add(Keys.Amount, $"{Amount.MinorFormat}");

            return result;
        }
    }
}
