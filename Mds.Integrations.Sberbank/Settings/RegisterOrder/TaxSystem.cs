﻿namespace Mds.Integrations.Sberbank.Settings.RegisterOrder
{
    public enum TaxSystem
    {
        Common = 0,
        SimplifiedIncome = 1,
        SimplifiedIncomeMinusChange = 2
    }
}
