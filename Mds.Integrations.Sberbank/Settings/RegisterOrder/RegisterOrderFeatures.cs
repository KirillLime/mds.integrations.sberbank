﻿using Mds.Integrations.Sberbank.Settings.Interfaces;

namespace Mds.Integrations.Sberbank.Settings.RegisterOrder
{
    public sealed class RegisterOrderFeatures : IValueParameter
    {
        public static readonly RegisterOrderFeatures AutoPayment = new RegisterOrderFeatures("AUTO_PAYMENT");
        public static readonly RegisterOrderFeatures ForseTds = new RegisterOrderFeatures("FORCE_TDS");

        internal RegisterOrderFeatures(string value)
        {
            Value = value;
        }

        public string Value { get; private set; }
    }
}
