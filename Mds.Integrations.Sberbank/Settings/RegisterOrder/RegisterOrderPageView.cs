﻿using Mds.Integrations.Sberbank.Settings.Interfaces;

namespace Mds.Integrations.Sberbank.Settings.RegisterOrder
{
    public sealed class RegisterOrderPageView : IValueParameter
    {
        public static readonly RegisterOrderPageView Desktop = new RegisterOrderPageView("DESKTOP");
        public static readonly RegisterOrderPageView Mobile = new RegisterOrderPageView("MOBILE");

        public RegisterOrderPageView(string value)
        {
            Value = value;
        }

        public string Value { get; private set; }
    }
}
