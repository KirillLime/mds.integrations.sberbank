﻿using Mds.Integrations.Sberbank.Settings.Interfaces;
using System.Collections.Generic;

namespace Mds.Integrations.Sberbank.Settings.RegisterOrder.Cart
{
    public class AgentInterest : IParameters
    {
        public AgentInterest() : this(string.Empty, 0) { }
        public AgentInterest(string type, int value)
        {
            Type = type;
            Value = value;
        }

        public string Type { get; set; }
        public int Value { get; set; }


        #region IParameters
        public Dictionary<string, object> CollectParameters()
        {
            var result = new Dictionary<string, object>();

            result.Add(Keys.Type, Type);
            result.Add(Keys.Value, $"{Value}");

            return result;
        }
        private static class Keys
        {
            public static readonly string Type = "interestType";
            public static readonly string Value = "interestValue";
        }
        #endregion
    }
}
