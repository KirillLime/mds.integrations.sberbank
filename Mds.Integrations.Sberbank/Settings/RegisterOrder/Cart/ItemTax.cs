﻿using Mds.Integrations.Sberbank.Settings.Interfaces;
using Mds.Integrations.Sberbank.Tools.Amount;
using System.Collections.Generic;

namespace Mds.Integrations.Sberbank.Settings.RegisterOrder.Cart
{
    public class ItemTax : IParameters
    {
        public ItemTax() : this(ItemTaxType.WithoutVat, 0d) { }
        public ItemTax(ItemTaxType type, double sum = 0d)
        {
            Type = type;
            Sum = new Price(sum);
        }

        public ItemTaxType Type { get; set; }
        public Price Sum { get; set; }


        #region IParameters
        public Dictionary<string, object> CollectParameters()
        {
            var result = new Dictionary<string, object>();

            result.Add(Keys.TaxType, (int)Type);
            result.Add(Keys.Sum, Sum.MinorFormat);

            return result;
        }
        private static class Keys
        {
            public static readonly string TaxType = "taxType";
            public static readonly string Sum = "taxSum";
        }
        #endregion
    }
}
