﻿using Mds.Integrations.Sberbank.Settings.Interfaces;
using System.Collections.Generic;

namespace Mds.Integrations.Sberbank.Settings.RegisterOrder.Cart
{
    public class ItemDetailsParam : IParameters
    {
        public ItemDetailsParam() : this(string.Empty, string.Empty) { }
        public ItemDetailsParam(string name, string value)
        {
            Name = name;
            Value = value;
        }

        public string Name { get; set; }
        public string Value { get; set; }


        #region IParameters
        public Dictionary<string, object> CollectParameters()
        {
            var result = new Dictionary<string, object>();

            result.Add(Keys.Name, Name);
            result.Add(Keys.Value, Value);

            return result;
        }
        private static class Keys
        {
            public static readonly string Name = "name";
            public static readonly string Value = "value";
        }
        #endregion
    }
}
