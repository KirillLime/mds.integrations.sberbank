﻿using Mds.Integrations.Sberbank.Settings.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Mds.Integrations.Sberbank.Settings.RegisterOrder.Cart
{
    public class CartItems : IParameters
    {
        public CartItems()
        {
            Items = new List<Item>();
        }
        public CartItems(IEnumerable<Item> items) : this()
        {
            Items.AddRange(items);
        }

        public List<Item> Items { get; set; }


        public void Add(Item item)
        {
            Items.Add(item);
        }


        #region IParameters
        public Dictionary<string, object> CollectParameters()
        {
            var result = new Dictionary<string, object>();

            var parameters = Items.Select(p => p.CollectParameters());
            result.Add(Keys.Items, parameters);

            return result;
        }
        private static class Keys
        {
            public static readonly string Items = "items";
        }
        #endregion
    }
}
