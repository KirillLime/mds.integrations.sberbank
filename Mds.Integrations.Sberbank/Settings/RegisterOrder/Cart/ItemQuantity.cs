﻿using Mds.Integrations.Sberbank.Settings.Interfaces;
using System.Collections.Generic;

namespace Mds.Integrations.Sberbank.Settings.RegisterOrder.Cart
{
    public class ItemQuantity : IParameters
    {
        public ItemQuantity() : this(0d, string.Empty) { }
        public ItemQuantity(double value, string measure)
        {
            Value = value;
            Measure = measure;
        }

        public double Value { get; set; }
        public string Measure { get; set; }


        #region IParameters
        public Dictionary<string, object> CollectParameters()
        {
            var result = new Dictionary<string, object>();

            result.Add(Keys.Value, Value);
            result.Add(Keys.Measure, Measure);

            return result;
        }

        private static class Keys
        {
            public static readonly string Value = "value";
            public static readonly string Measure = "measure";
        }
        #endregion
    }
}
