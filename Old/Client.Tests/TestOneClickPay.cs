﻿using Mds.Integrations.Sberbank.Responses;
using Mds.Integrations.Sberbank.Settings;
using Mds.Integrations.Sberbank.Settings.GetOrderStatus;
using Mds.Integrations.Sberbank.Settings.PaymentOrderBinding;
using Mds.Integrations.Sberbank.Settings.RegisterOrder;
using Mds.Integrations.Sberbank.Tools;
using Mds.Integrations.Sberbank.Tools.Amount;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace Mds.Integrations.Sberbank.Tests
{
    [TestClass]
    public class TestOneClickPay
    {
        private readonly string userName;
        private readonly string password;
        private readonly bool sandbox;
        private readonly string returnUrl;

        private readonly AuthSettings authSettings;
        private readonly SberbankClient sberbankClient;

        public TestOneClickPay()
        {
            var data = JObject.Parse(File.ReadAllText("config.json"));
            userName = data["userName"].ToString();
            password = data["password"].ToString();
            sandbox = data["sandbox"].ToObject<bool>();
            returnUrl = data["returnUrl"].ToString();

            authSettings = new AuthSettings(userName, password);
            sberbankClient = new SberbankClient(authSettings);
            sberbankClient.SandboxMode = sandbox;
        }

        [TestMethod]
        public async Task TestOneClick()
        {
            var clientId = "234";

            #region create first order
            var parameters = new RegisterOrderParameters(clientId, new Price(1, 0), returnUrl, Currency.RUB);
            var orderResponse = await sberbankClient.RegisterOrderAsync(parameters);
            if (!orderResponse.IsSuccess)
            {
                throw new Exception($"Регистрация заказа отменена. Причина: {orderResponse.ErrorMessage}");
            }


            var statusParameters = new GetOrderStatusParameters(orderResponse.OrderId);
            var statusResponse = await sberbankClient.GetOrderStatusAsync(statusParameters);
            if (!statusResponse.IsSuccess || statusResponse.OrderStatus != OrderStatus.AuthorizedOrCompleted)
            {
                throw new Exception($"Заказ не обработан. Причина: {statusResponse.ErrorMessage}");
            }

            var bindingId = statusResponse.BindingId;
            File.AppendAllText("Cached_Binding.txt", $"{orderResponse.OrderId}:{bindingId}\n");

            try
            {
                await Refund(orderResponse.OrderId, parameters.Amount);
            }
            catch
            {
                await Reverse(orderResponse.OrderId);
            }
            #endregion    

            #region create second order
            var secondOrderParameters = new RegisterOrderParameters(clientId, bindingId, new Price(5, 00), Currency.RUB);
            var secondOrderResponse = await sberbankClient.RegisterOrderAsync(secondOrderParameters);
            if (!secondOrderResponse.IsSuccess)
            {
                var message = $"Регистрация второго заказа была отменена. Причина: {secondOrderResponse.ErrorMessage}";
                Debug.WriteLine(message);
                throw new Exception(message);
            }
            File.AppendAllText("Cached_Binding.txt", $"{secondOrderResponse.OrderId}\n");


            var paymentOrderBindingParameters = new PaymentOrderBindingParameters(secondOrderResponse.OrderId, bindingId, statusResponse.Ip);
            var completePaymentResponse = await sberbankClient.PaymentOrderBindingAsync(paymentOrderBindingParameters);


            var status = await CheckStatusAsync(secondOrderResponse.OrderId);
            if (status != OrderStatus.AuthorizedOrCompleted)
            {
                throw new Exception("Связанный платеж не прошел");
            }
            #endregion
        }
        private async Task<OrderStatus> CheckStatusAsync(string orderId)
        {
            var statusResponse = await sberbankClient.GetOrderStatusAsync(orderId);
            if (!statusResponse.IsSuccess)
            {
                throw new Exception($"Статус заказа недоступен. Причина: {statusResponse.ErrorMessage}");
            }

            return statusResponse.OrderStatus;
        }
        private async Task<ReverseResponse> Reverse(string orderId)
        {
            var reverseResponse = await sberbankClient.ReverseAsync(orderId);
            if (!reverseResponse.IsSuccess)
            {
                throw new Exception($"Заказ не был отменен. Причина: {reverseResponse.ErrorMessage}");
            }

            return reverseResponse;
        }
        private async Task<RefundResponse> Refund(string orderId, Price amount)
        {
            var refundResponse = await sberbankClient.RefundAsync(orderId, amount);
            if (!refundResponse.IsSuccess)
            {
                throw new Exception($"Заказ не был отменен. Причина: {refundResponse.ErrorMessage}");
            }

            return refundResponse;
        }

        [TestMethod]
        public async Task TestRefund()
        {
            var orderId = "1778ec64-86c6-7404-1778-ec640014fd50";

            var refundResponse = await sberbankClient.RefundAsync(orderId, new Price(1, 0));
            if (!refundResponse.IsSuccess)
            {
                throw new Exception($"Заказ не был отменен. Причина: {refundResponse.ErrorMessage}");
            }
        }


        [TestMethod]
        public void TestOrderStatusAsync()
        {
            var orderId = "94ce2b49-829e-731b-94ce-2b4904b02571";

            var statusResponse = sberbankClient.GetOrderStatus(orderId);
            if (!statusResponse.IsSuccess)
            {
                throw new Exception($"Статус заказа недоступен. Причина: {statusResponse.ErrorMessage}");
            }
        }
    }
}
