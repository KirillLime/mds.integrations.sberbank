﻿using Mds.Integrations.Sberbank.Settings.Interfaces;

namespace Mds.Integrations.Sberbank.Settings.RegisterOrder
{
    public sealed class RegisterOrderPageView : IValueParameter
    {
        public static readonly RegisterOrderPageView Desktop = new RegisterOrderPageView("DESKTOP");
        public static readonly RegisterOrderPageView Mobile = new RegisterOrderPageView("MOBILE");

        public string Value { get; private set; }
        public RegisterOrderPageView(string value)
        {
            Value = value;
        }
    }
}
