﻿using Mds.Integrations.Sberbank.Settings.Interfaces;

namespace Mds.Integrations.Sberbank.Settings.RegisterOrder
{
    public sealed class RegisterOrderFeatures : IValueParameter
    {
        public static readonly RegisterOrderFeatures AutoPayment = new RegisterOrderFeatures("AUTO_PAYMENT");


        public string Value { get; private set; }
        internal RegisterOrderFeatures(string value)
        {
            Value = value;
        }
    }
}
