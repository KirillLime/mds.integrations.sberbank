﻿namespace Mds.Integrations.Sberbank.Settings.Interfaces
{
    public interface IValueParameter
    {
        string Value { get; }
    }
}
