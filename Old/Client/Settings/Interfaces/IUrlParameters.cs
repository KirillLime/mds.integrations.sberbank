﻿using System.Collections.Generic;

namespace Mds.Integrations.Sberbank.Settings.Interfaces
{
    public interface IUrlParameters
    {
        Dictionary<string, string> CollectParameters();
    }
}
