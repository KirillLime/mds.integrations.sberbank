﻿using Mds.Integrations.Sberbank.Responses;
using Mds.Integrations.Sberbank.Settings;
using Mds.Integrations.Sberbank.Settings.PaymentOrderBinding;
using Mds.Integrations.Sberbank.Settings.RegisterOrder;
using Mds.Integrations.Sberbank.Settings.RegisterOrder.Cart;
using Mds.Integrations.Sberbank.Tools;
using Mds.Integrations.Sberbank.Tools.Amount;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace Mds.Integrations.Sberbank.Tests
{
    [TestClass]
    public class Sandbox
    {
        private readonly string returnUrl;

        private readonly AuthSettings authSettings;
        private readonly SberbankClient sberbankClient;

        public Sandbox()
        {
            var data = JObject.Parse(File.ReadAllText("config.json"));
            var username = data["username"].ToString();
            var password = data["password"].ToString();
            var sandbox = data["sandbox"].ToObject<bool>();
            returnUrl = data["returnUrl"].ToString();

            authSettings = new AuthSettings(username, password);
            sberbankClient = new SberbankClient(authSettings);
            sberbankClient.SandboxMode = sandbox;
        }

        [TestMethod]
        public async Task TestOneClick()
        {
            var clientId = "234";

            //#region create first order
            //var parameters = new RegisterOrderParameters(clientId, new Price(1, 0), returnUrl, Currency.RUB);
            //var orderResponse = await sberbankClient.RegisterOrderAsync(parameters);
            //if (!orderResponse.IsSuccess)
            //{
            //    throw new Exception($"Регистрация заказа отменена. Причина: {orderResponse.ErrorMessage}");
            //}


            //var statusParameters = new GetOrderStatusParameters(orderResponse.OrderId);
            //var statusResponse = await sberbankClient.GetOrderStatusAsync(statusParameters);
            //if (!statusResponse.IsSuccess || statusResponse.OrderStatus != OrderStatus.AuthorizedOrCompleted)
            //{
            //    throw new Exception($"Заказ не обработан. Причина: {statusResponse.ErrorMessage}");
            //}

            //var bindingId = statusResponse.BindingId;
            //var sourceIp = statusResponse.Ip;

            //File.AppendAllText("Cached_Binding.txt", $"{orderResponse.OrderId}:{bindingId}\n");


            //try
            //{
            //    await Refund(orderResponse.OrderId, parameters.Amount);
            //}
            //catch
            //{
            //    await Reverse(orderResponse.OrderId);
            //}
            //#endregion    


            var bindingId = "0c1db9c5-b035-40c7-90bc-82fe2ef4cb4e";
            var sourceIp = "90.189.153.104";

            #region create second order
            var bundle = new OrderBundle()
            {
                CreatedAt = DateTime.UtcNow,
                Details = new CustomerDetails()
                {
                    Email = "a.shumakov@medved.studio",
                    Contact = "Email"
                },
                Items = new CartItems()
                {
                    Items = new List<Item>()
                    {
                        new Item()
                        {
                            PositionId = 1,
                            Code = Guid.NewGuid().ToString(),
                            Name = Guid.NewGuid().ToString(),
                            Quantity = new ItemQuantity(2, "шт"),
                            Price = new Price(2.5),
                            Amount = new Price(5d),
                            Tax = new ItemTax(ItemTaxType.WithoutVat, 0)
                        }
                    }
                }
            };
            var order = new RegisterOrderParameters(clientId, bindingId, new Price(5, 00), Currency.RUB)
            {
                OrderBundle = bundle,
                TaxSystem = TaxSystem.SimplifiedIncomeMinusChange
            };

            var secondOrderResponse = await sberbankClient.RegisterOrderAsync(order);
            if (!secondOrderResponse.IsSuccess)
            {
                var message = $"Регистрация второго заказа была отменена. Причина: {secondOrderResponse.ErrorMessage}";
                Debug.WriteLine(message);
                throw new Exception(message);
            }
            File.AppendAllText("Cached_Binding.txt", $"{secondOrderResponse.OrderId}\n");


            var paymentOrderBindingParameters = new PaymentOrderBindingParameters(secondOrderResponse.OrderId, bindingId, sourceIp);
            var completePaymentResponse = await sberbankClient.PaymentOrderBindingAsync(paymentOrderBindingParameters);


            var status = await CheckStatusAsync(secondOrderResponse.OrderId);
            if (status != OrderStatus.AuthorizedOrCompleted)
            {
                throw new Exception("Связанный платеж не прошел");
            }
            #endregion
        }
        private async Task<OrderStatus> CheckStatusAsync(string orderId)
        {
            var statusResponse = await sberbankClient.GetOrderStatusAsync(orderId);
            if (!statusResponse.IsSuccess)
            {
                throw new Exception($"Статус заказа недоступен. Причина: {statusResponse.ErrorMessage}");
            }

            return statusResponse.OrderStatus;
        }
        private async Task<ReverseResponse> Reverse(string orderId)
        {
            var reverseResponse = await sberbankClient.ReverseAsync(orderId);
            if (!reverseResponse.IsSuccess)
            {
                throw new Exception($"Заказ не был отменен. Причина: {reverseResponse.ErrorMessage}");
            }

            return reverseResponse;
        }
        private async Task<RefundResponse> Refund(string orderId, Price amount)
        {
            var refundResponse = await sberbankClient.RefundAsync(orderId, amount);
            if (!refundResponse.IsSuccess)
            {
                throw new Exception($"Заказ не был отменен. Причина: {refundResponse.ErrorMessage}");
            }

            return refundResponse;
        }

        [TestMethod]
        public async Task TestSell()
        {
            var clientId = "123";

            var bundle = new OrderBundle()
            {
                CreatedAt = DateTime.UtcNow,
                Details = new CustomerDetails()
                {
                    Email = "kt@lime-it.ru",
                    Contact = "Email"
                },
                Items = new CartItems()
                {
                    Items = new List<Item>()
                    {
                        new Item()
                        {
                            PositionId = 1,
                            Code = Guid.NewGuid().ToString(),
                            Name = Guid.NewGuid().ToString(),
                            Quantity = new ItemQuantity(2, "шт"),
                            Price = new Price(2.5),
                            Amount = new Price(5d),
                            Tax = new ItemTax(ItemTaxType.WithoutVat, 0)
                        }
                    }
                }
            };
            var order = new RegisterOrderParameters(clientId, new Price(5, 00), "https://lime-it.ru/", Currency.RUB)
            {
                OrderBundle = bundle,
                TaxSystem = TaxSystem.SimplifiedIncomeMinusChange
            };

            var orderResponse = await sberbankClient.RegisterOrderAsync(order);
            if (!orderResponse.IsSuccess)
            {
                var message = $"Регистрация второго заказа была отменена. Причина: {orderResponse.ErrorMessage}";
                Debug.WriteLine(message);
                throw new Exception(message);
            }

            var paymentOrderBindingParameters = new PaymentOrderBindingParameters()
            {
                OrderId = orderResponse.OrderId
                
            };
            var completePaymentResponse = await sberbankClient.PaymentOrderBindingAsync(paymentOrderBindingParameters);


            var status = await CheckStatusAsync(orderResponse.OrderId);
            if (status != OrderStatus.AuthorizedOrCompleted)
            {
                throw new Exception("Связанный платеж не прошел");
            }

        }


        [TestMethod]
        public async Task TestRefund()
        {
            var orderId = "1778ec64-86c6-7404-1778-ec640014fd50";

            var refundResponse = await sberbankClient.RefundAsync(orderId, new Price(1, 0));
            if (!refundResponse.IsSuccess)
            {
                throw new Exception($"Заказ не был отменен. Причина: {refundResponse.ErrorMessage}");
            }
        }


        [TestMethod]
        public void TestOrderStatusAsync()
        {
            var orderId = "94ce2b49-829e-731b-94ce-2b4904b02571";

            var statusResponse = sberbankClient.GetOrderStatus(orderId);
            if (!statusResponse.IsSuccess)
            {
                throw new Exception($"Статус заказа недоступен. Причина: {statusResponse.ErrorMessage}");
            }
        }
    }
}
