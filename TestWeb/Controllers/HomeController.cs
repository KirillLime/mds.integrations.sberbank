﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Mds.Integrations.Sberbank;
using Mds.Integrations.Sberbank.Responses;
using Mds.Integrations.Sberbank.Settings;
using Mds.Integrations.Sberbank.Settings.PaymentOrderBinding;
using Mds.Integrations.Sberbank.Settings.RegisterOrder;
using Mds.Integrations.Sberbank.Settings.RegisterOrder.Cart;
using Mds.Integrations.Sberbank.Tools;
using Mds.Integrations.Sberbank.Tools.Amount;
using Microsoft.AspNetCore.Mvc;
using TestWeb.Models;

namespace TestWeb.Controllers
{
    public class HomeController : Controller
    {
        private AuthSettings authSettings;
        private SberbankClient sberbankClient;
        private RegisterOrderResponse orderResponse;

        public async Task<IActionResult> SellAsync()
        {
            var username = "park-api";
            var password = "park";
            var returnUrl = "http://localhost:57929/Home/ResultAsync";

            authSettings = new AuthSettings(username, password);
            sberbankClient = new SberbankClient(authSettings);
            sberbankClient.SandboxMode = true;

            var clientId = "123";

            var bundle = new OrderBundle()
            {
                CreatedAt = DateTime.UtcNow,
                Details = new CustomerDetails()
                {
                    Email = "kt@lime-it.ru",
                    Contact = "Email"
                },
                Items = new CartItems()
                {
                    Items = new List<Item>()
                    {
                        new Item()
                        {
                            PositionId = 1,
                            Code = Guid.NewGuid().ToString(),
                            Name = Guid.NewGuid().ToString(),
                            Quantity = new ItemQuantity(2, "шт"),
                            Price = new Price(2.5),
                            Amount = new Price(5d),
                            Tax = new ItemTax(ItemTaxType.WithoutVat, 0)
                        }
                    }
                }
            };
            var order = new RegisterOrderParameters(clientId, new Price(5, 00), returnUrl, Currency.RUB)
            {
                Features = RegisterOrderFeatures.ForseTds,
                OrderBundle = bundle,
                TaxSystem = TaxSystem.SimplifiedIncomeMinusChange
            };

            orderResponse = await sberbankClient.RegisterOrderAsync(order);
            if (!orderResponse.IsSuccess)
            {
                var message = $"Регистрация второго заказа была отменена. Причина: {orderResponse.ErrorMessage}";
                Debug.WriteLine(message);
                throw new Exception(message);
            }

            return Redirect(orderResponse.FormUrl);
        }

        private async Task<OrderStatus> CheckStatusAsync(string orderId)
        {
            var statusResponse = await sberbankClient.GetOrderStatusAsync(orderId);
            if (!statusResponse.IsSuccess)
            {
                throw new Exception($"Статус заказа недоступен. Причина: {statusResponse.ErrorMessage}");
            }

            return statusResponse.OrderStatus;
        }
        private async Task<ReverseResponse> Reverse(string orderId)
        {
            var reverseResponse = await sberbankClient.ReverseAsync(orderId);
            if (!reverseResponse.IsSuccess)
            {
                throw new Exception($"Заказ не был отменен. Причина: {reverseResponse.ErrorMessage}");
            }

            return reverseResponse;
        }
        private async Task<RefundResponse> Refund(string orderId, Price amount)
        {
            var refundResponse = await sberbankClient.RefundAsync(orderId, amount);
            if (!refundResponse.IsSuccess)
            {
                throw new Exception($"Заказ не был отменен. Причина: {refundResponse.ErrorMessage}");
            }

            return refundResponse;
        }


        public async Task<IActionResult> ResultAsync(string orderId)
        {

            var paymentOrderBindingParameters = new PaymentOrderBindingParameters()
            {
                OrderId = orderId

            };

            var username = "park-api";
            var password = "park";

            authSettings = new AuthSettings(username, password);
            sberbankClient = new SberbankClient(authSettings);
            sberbankClient.SandboxMode = true;

            var completePaymentResponse = await sberbankClient.PaymentOrderBindingAsync(paymentOrderBindingParameters);


            var status = await CheckStatusAsync(orderId);
            if (status != OrderStatus.AuthorizedOrCompleted)
            {
                throw new Exception("Связанный платеж не прошел");
            }

            ViewData["Status"] = status;

            return View();
        }

        public IActionResult Index()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
